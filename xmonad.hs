import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.CustomKeys (customKeys)
import XMonad.Util.EZConfig(additionalKeys)
import System.IO
import XMonad.Util.SpawnOnce
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.CycleWS
--
import Graphics.X11.ExtraTypes.XF86 ( xF86XK_AudioPrev
                                    , xF86XK_AudioPlay
                                    , xF86XK_AudioNext
                                    , xF86XK_AudioMute
                                    , xF86XK_AudioLowerVolume
                                    , xF86XK_AudioRaiseVolume
                                    , xF86XK_MonBrightnessDown
                                   , xF86XK_MonBrightnessUp)
-- a basic CycleWS setup

-- main = do
--     xmonad $ defaultConfig
main = do
    -- xmproc <- spawnPipe "xmobar"
    xmonad $ defaultConfig 
       { borderWidth        = 0
       , modMask = mod4Mask
       , terminal           = "alacritty"
       , normalBorderColor  = "#cccccc"
       , focusedBorderColor = "#0"
       , keys = customKeys delkeys inskeys
       , manageHook         = manageDocks <+> manageHook defaultConfig
       , layoutHook         = avoidStruts  $ layoutHook defaultConfig
       -- this must be in this order, docksEventHook must be last
       , handleEventHook    = handleEventHook defaultConfig <+> docksEventHook
       -- , logHook            = dynamicLogWithPP xmobarPP
       --     {
       --     ppOutput          = hPutStrLn xmproc
       --     , ppTitle           = xmobarColor "darkgreen"  "" . shorten 20
       --     , ppHiddenNoWindows = xmobarColor "grey" ""
       --     }
       , startupHook  = startup
      }


altMask = mod1Mask 

inskeys :: XConfig l -> [((KeyMask, KeySym), X ())]
inskeys conf@XConfig {modMask = modMask} =
  [ -- modMask + alt + space
  -- ((modMask .|. altMask, xK_space), spawn "incr_luminosity.sh")
  -- , ((modMask .|. altMask, xK_space), spawn "decr_luminosity.sh")
  ((0, xF86XK_AudioRaiseVolume), spawn "incr_volume.sh")
  , ((0, xF86XK_AudioLowerVolume), spawn "decr_volume.sh")
  , ((0, xF86XK_MonBrightnessUp  ), spawn "incr_luminosity.sh")
  , ((0, xF86XK_MonBrightnessDown), spawn "decr_luminosity.sh")
--  -- , ((modMask,               xK_b        ), spawn "firefox")
  -- , ((modMask, xK_d), spawn "incr_luminosity.sh")
  -- , ((modMask, xK_b), spawn "decr_luminosity.sh")
  , ((altMask,               xK_Right),  nextWS)
  , ((altMask,               xK_Left),    prevWS)
  , ((altMask .|. shiftMask, xK_Right),  shiftToNext)
  , ((altMask .|. shiftMask, xK_Left),    shiftToPrev)
  , ((altMask,               xK_Right), nextScreen)
  , ((altMask,               xK_Left),  prevScreen)
  , ((altMask .|. shiftMask, xK_Right), shiftNextScreen)
  , ((altMask .|. shiftMask, xK_Left),  shiftPrevScreen)
  , ((altMask,               xK_z),     toggleWS)
  ]

delkeys :: XConfig l -> [(KeyMask, KeySym)]
delkeys XConfig {} = []

--
-- with startupHook = 
--
-- this is totally untested but i guess startup just needs to be :: X ()
-- which runOrRaise also happens to return...
startup :: X ()
startup = do
    spawn "xmobar"
    runOrRaise "firefox" (className =? "firefox")
    runOrRaise "emacs" (className =? "emacs")
